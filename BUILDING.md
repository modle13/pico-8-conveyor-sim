# build from Dockerfile

```
docker build -t registry.gitlab.com/modle13/pico-8-conveyor-sim .
```

# push to remote

```
docker push registry.gitlab.com/modle13/pico-8-conveyor-sim
```

# run with compose

```
docker-compose up
```
