#FROM xvfb-runner:latest
FROM ubuntu:latest

# since pico-8 is a paid-for application, the binary must be provided via a mount path
#xvfb-run -a -s \"-screen 0 1400x900x24 +extension RANDR\" -- ./pico8 -export conveyor.html conveyor.p8"


RUN apt-get update -y
RUN apt-get install -y git zip
RUN apt-get install -y libsdl2-2.0

# noninteractive is needed by tzdata install
ENV DEBIAN_FRONTEND=noninteractive
# tzdata would get installed as part of xorg-dev
RUN apt-get install -y tzdata

RUN apt-get install -y xorg-dev libglu1-mesa libgl1-mesa-dev libxinerama1 libxcursor1

# xvfb is the wrapper used to run apps requiring x11 utils
RUN apt-get install -y xvfb
