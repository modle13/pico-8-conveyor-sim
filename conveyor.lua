-- title: conveyor belt simulator
-- author: modle
-- description:
--     this is prototype/reference code
--     beware inefficiencies/inconsistencies

function _init()
    cam1=cam:new()
    set_room(0,0)

    create_cursors()
    player1=player:new()
    player_dummy=add_actor(60,10,8,'player')
    temp_player_x,temp_player_y=player1.x,player1.y
    state.reference_belt=instantiate_belt(20000,20000)

    inv_itm=inv:new('inventory',false)
    inv_tl=inv:new('tools',true)
    inv_tl:add('building')
    inv_tl:add('belt')
    inv_tl:add('delete')

    -- start music
    -- music(2)
    get_wave_cells()

    --make_building(1,6.5,3.5)
    --make_building(23,7.5,3.5)

    --make_building(1,6.5,5.5)
    --make_building(3,7.5,5.5)
    --make_building(23,8.5,5.5)

    --make_building(1,6.5,7.5)
    --make_building(5,7.5,7.5)
    --make_building(23,8.5,7.5)

    -- export_map_sprites()
end

function _update()
    if (state.tutorial_enabled) return
    handle_button_presses()
    check_menu_ops()

    if (state.tools_on) inv_tl:update() inv_itm:update() else player1:control() cursor1:control()

    foreach(state.products,move_actor)

    foreach(state.production_structures,update_building)

    foreach(state.room_map_sprites,animate_structure)

    update_reference_sprites()
    foreach(state.wave_cells,update_map_cells)
    manage_frame()
    update_product_pos_map()
end

function _draw()
    -- clear screen
    cls()

    if state.tutorial_enabled then
        map(112,0,0,0,16,16)
        draw_tutorial()
    else
        map(0,0,0,0,16,16)
        map(glb.structure_map_x,0,0,0,16,16)

        handle_coordinates_debug()
        foreach(state.visible_sprites,draw_actor)
        player1:draw()

        if (state.tools_on) inv_tl:draw() inv_itm:draw()
        -- need another mode here: grid or build, only draw cursor in this mode
        draw_ui_sprite(cursor1,'cursor')
    end

    if (state.dialog_mode) draw_dialog()

    cam1:update()
end
