function animate_structure(a)
    -- why 0.25? quarter of a square
    a.frame+=0.25
    a.frame%=a.frames
    if a.room==state.room then
        mset(a.rel_x+glb.structure_map_x,a.rel_y,a.sprite_id+a.frame)
    end
end

function manage_frame()
    state.reference_frame+=1
    state.reference_frame%=glb.max_frames
end

function update_reference_sprites()
    state.reference_wave.frame+=0.0625
    state.reference_wave.frame%=state.reference_wave.frames
    state.reference_belt.frame+=0.25
    state.reference_belt.frame%=state.reference_belt.frames
    -- state.reference_building.frame += 0.0625
    -- state.reference_building.frame %= state.reference_building.frames
end
