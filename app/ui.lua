function draw_ui_sprite(a, name)
    x = a.x
    y = a.y
    local sx = x * glb.cell_size
    local sy = y * glb.cell_size
    spr(a.sprite_id + a.frame, sx, sy, 1, 1, a.direction_x==-1, a.direction_y==-1)
end

function draw_dialog()
    -- can params be split/a table?
    map(97,28,8,96,14,3)
    print('you are me... what is this...',12,100)
end

function showtext(txt,x,y,clr)
    local endx=print(txt,0,-10)
    map(97,28,x-endx/2,y-1,flr(endx/8+0.5),1)
    print(txt,x-endx/2,y,clr)
end
