player={}

function player:new()
    local o=actor:new(glb.player_sprite_id,8,8,'player')
    setmetatable(o,self)
    self.__index = self
    o.draw=actor.draw
    o.move=actor.move
    o.update_room=actor.update_room
    o.w=0.4
    o.h=0.4
    o.frames=4
    return o
end

function player:handle_player_target()
    x=self.x
    y=self.y
    target_sprite=check_for_structure(x, y)
    if target_sprite and contains(player_targets, target_sprite) then
        printh('handling player target ' .. target_sprite)
    end
end

function player:control()
    if (btn(5)) return
    accel=0.05
    if (btn(0)) self.dx-=accel self.direction_x=-1
    if (btn(1)) self.dx+=accel self.direction_x=1
    if (btn(2)) self.dy-=accel self.direction_y=-1
    if (btn(3)) self.dy+=accel self.direction_y=1
    state.dialog_mode=actor_collide(self,player_dummy,'bubble',0,0)
    self.move(self)
end
