function check_menu_ops()
    if(btn(6)) poke(0x5f30,1) toggle_tools() --suppress pause
    if state.tools_on then
        if(btnp(0) and not state.inv_on and not is_empty(inv_itm.data)) state.inv_on=true inv_tl:off() inv_itm:on()
        if(btnp(1) and state.inv_on) state.inv_on=false inv_tl:on() inv_itm:off()
        if(btnp(4) and not state.inv_on) cursor1:change_to_selector(inv_tl.cur) state.tools_on=false
    end
end

function toggle_tools()
    state.tools_on=not state.tools_on
end

function toggle_mode(mode)
    if(mode=='tutorial')toggle_tutorial()
end

function toggle_tutorial()
    state.tutorial_enabled=not state.tutorial_enabled
    if (not state.tutorial_enabled) clean_up_tutorial_sprites()
    manage_tutorial_entry()
end

function manage_tutorial_entry()
    if (state.tutorial_enabled) then tutorial_str="exit tutorial" else tutorial_str="open tutorial" end
    menuitem(4,tutorial_str,function() toggle_mode("tutorial") end)
end

manage_tutorial_entry()
menuitem(2,"toggle counters",function() state.show_counts=not state.show_counts end)
-- menuitem(3, "toggle coords",   function() state.show_coords = not state.show_coords end)
