function get_belt_effect(actor)
    local belt=in_quad(actor,state.occupied_belt)
    direction='none'
    belt_dir_x=0
    belt_dir_y=0
    if belt then
        belt_speed=0.02
        direction=get_belt_direction(belt)
        if (direction == 'up') belt_dir_y=-belt_speed actor.direction_y=-1 actor.direction_x=0
        if (direction == 'down') belt_dir_y=belt_speed actor.direction_y=1  actor.direction_x=0
        if (direction == 'left') belt_dir_x=-belt_speed actor.direction_x=-1 actor.direction_y=0
        if (direction == 'right') belt_dir_x=belt_speed actor.direction_x=1  actor.direction_y=0
        -- if actor is not centered on belt, apply some movement toward the center depending on current orientation
        off_horizontal,off_vertical = check_belt_centering(belt,actor)
        if (direction == 'up' and off_horizontal != 0) belt_dir_x=belt_dir_x+belt_speed*off_horizontal
        if (direction == 'down' and off_horizontal != 0) belt_dir_x=belt_dir_x+belt_speed*off_horizontal
        if (direction == 'left' and off_vertical != 0) belt_dir_y=belt_dir_y+belt_speed*off_vertical
        if (direction == 'right' and off_vertical != 0) belt_dir_y=belt_dir_y+belt_speed*off_vertical
    end
    return belt_dir_x,belt_dir_y
end

function check_belt_centering(belt,actor)
    off_horizontal=0
    off_vertical=0
    -- use a margin to avoid jitter
    margin=0.2
    if (actor.x-0.5+margin<belt.x) off_horizontal=1
    if (actor.x-0.5-margin>belt.x) off_horizontal=-1
    if (actor.y-0.5+margin<belt.y) off_vertical=1
    if (actor.y-0.5-margin>belt.y) off_vertical=-1
    return off_horizontal,off_vertical
end

function make_belt(x,y,options)
    local new_belt=instantiate_belt(x,y,options)
    add(state.structures,new_belt)
    add(state.room_map_sprites,new_belt)
    pos_val=prpos(getvec(new_belt))
    state.occupied_belt[pos_val]=new_belt
end

function instantiate_belt(x,y,options)
    local flip_x=1
    local flip_y=1
    local sprite_id=12
    if (options and options.direction=='right') then
        sprite_id=28
    end
    if (options and options.direction=='down') then
        flip_y=-1
        sprite_id=44
    end
    if (options and options.direction=='left') then
        flip_x=-1
        sprite_id=60
    end
    local x_pos=x+glb.MAP_W*state.room_x
    local y_pos=y+glb.MAP_H*state.room_y
    a=actor:new(sprite_id,x_pos,y_pos,'belt')
    a.frames=4
    a.stationary=true
    a.direction_x=flip_x
    a.direction_y=flip_y
    a.direction=get_belt_direction(a)
    if (state.reference_belt~=nil) a.frame=state.reference_belt.frame
    return a
end

function orient_belt(x,y)
    printh('checking for belt at '..prpos(x,y))
    local belt=find_structure(x,y)
    if (belt==nil) printh('error getting belt at '..prpos(x,y)) return
    new_direction=cursor1.direction
    set_belt_direction(new_direction,belt)
end

function get_belt_direction(belt)
    current_dir='up'
    -- 28 is the horizontal sprite
    -- use vars for these sprite ids, in case they move on the sheet
    if (belt.sprite_id==28) current_dir='right'
    if (belt.sprite_id==44) current_dir='down'
    if (belt.sprite_id==60) current_dir='left'
    return current_dir
end

function set_belt_direction(direction,belt)
    -- defaults to up
    belt.direction_x=1
    belt.direction_y=1
    belt.sprite_id=12
    if direction=='right' then
        belt.sprite_id=28
    elseif direction=='down' then
        belt.sprite_id=44
        belt.direction_y=-1
    elseif direction=='left' then
        belt.sprite_id=60
        belt.direction_x=-1
    end
end
