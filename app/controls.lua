function handle_button_presses()
    if (state.tools_on) return
    if btn(5) then
        if (btnp(2) or btnp(3)) cycle_cursor_type(btnp(2)) return
        if (btnp(0) or btnp(1)) cycle_selection_sprite(cursor1.name,btnp(0)) return
    end
    if (btnp(4)) cursor1:handle_target()
end
