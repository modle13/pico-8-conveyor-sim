inv={}

function inv:new(name,on)
    local o={}
    o.data={}
    o.name=name
    o.cur=nil
    o.enabled=on
    setmetatable(o,self)
    self.__index=self
    return o
end

function inv:add(name)
    local _,a=contains_attr(self.data,'name',name)
    if (a!=nil) a.count+=1 return
    local x=glb.inv_x
    local spr_map=production_sprite_map
    printh('self.name is '..self.name..' name is '..name)
    if (self.name=='tools') x=glb.tool_x spr_map=cursor_spr_map
    local a=actor:new(spr_map[name],x,glb.inv_y+#self.data*2,name)
    a:update_label()
    a.count=1
    a.backing={10,11,26,27}
    a.backing_big={42,43,58,59}
    add(self.data,a)
    if (self.cur==nil) self.cur=a
    if (self.enabled and self.cur==a) a.selected=true
end

function inv:draw()
    foreach(self.data,draw_actor)
    if (self.name=='tools' and state.inv_on) return
    if (self.name=='inventory' and not state.inv_on) return
    showtext(self.name,8*glb.cell_size,13*glb.cell_size,7)
end

function inv:update()
    if (is_empty(self.data)) return
    if (self.name=='tools' and state.inv_on) return
    if (self.name=='inventory' and not state.inv_on) return
    if (btnp(2) or btnp(3)) self:cycle()
    if (btnp(1) and self.name=='tools') then
        _,self.cur.sprite_id=getnext(self.cur.sprite_id,cursors[self.cur.name],false)
        self.cur:update_label()
    end
end

function inv:cycle()
    local k,cur=contains_attr(self.data,'selected',true)
    local j,nxt=getnext(k,self.data,btnp(2),true)
    if nxt!=nil and cur!=nxt then
        cur.selected=false
        nxt.selected=true
        self.cur=nxt
        local dir=1
        if (btnp(2)) dir=-1
        for i,v in pairs(self.data) do v.rel_y=glb.inv_y+(i-j)*2 end --pos shift
    end
end

function inv:on() self.enabled=true if(self.cur!=nil) self.cur.selected=true end
function inv:off() self.enabled=false self.cur.selected=false end
