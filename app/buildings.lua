function update_building(bldg)
    -- check for material in input tile
        -- if found, and if need input, consume
    -- if can produce, check for product in output tile
        -- output tile will always be to right of structure, can change later
        -- if found, do nothing
        -- if not found, make one
    -- state.reference_frame == 0 will cause this to trigger every 60 frames (2 seconds)
    if state.reference_frame!=0 then
        return
    end

    local consumes=bldg.consumes
    local produces=bldg.produces
    -- set material true by default to account for structures that don't have an input req
    if consumes!='' then
        if bldg.material==nil then
            check_for_mat(bldg)
        end
    end
    if bldg.label=='storage' and bldg.material!=nil then
        -- then this is a counter/storage
        inv_itm:add(bldg.material)
        if (state.sfx_enabled) sfx(35)
        bldg.material=nil
        return
    end
    if produces!=nil then
        if bldg.material==nil and consumes!='' then
            printh('no input material found for ' .. produces .. ' production at ' .. bldg.name .. ' ' .. bldg.label)
        else
            produce(bldg)
        end
    end
end

function check_for_mat(bldg)
    local found=nil
    local input=cell_occupied(bldg,'bubble')
    if (input!=nil and (bldg.consumes==input.name or bldg.consumes=='all')) found=input 
    if (found!=nil) bldg.material=found.name delete_mat(found)
end

function delete_mat(mat)
    del(state.actors,mat)
    del(state.products,mat)
    del(state.visible_sprites,mat)
end

function make_building(sprite_id,x,y)
    local x_pos=x+glb.MAP_W*state.room_x
    local y_pos=y+glb.MAP_H*state.room_y
    local bldg=actor:new(sprite_id,x_pos,y_pos,'building')
    bldg.w,bldg.h=0.5,0.5
    -- change to 'mat'
    bldg.material=nil
    set_building_traits(bldg,sprite_id)
    add(state.structures,bldg)
    add(state.production_structures,bldg)
    add(state.room_map_sprites,bldg)
end

function produce(bldg)
    if solid(bldg.x+1,bldg.y,'placement',bldg.room) then
        printh('not eligible to create product at '..prpos(bldg.x+1,bldg.y))
        return
    end
    if cell_occupied(bldg,'right')!=nil then
        printh(bldg.label..' cannot produce; output occupied ')
        return
    end
    product=actor:new(production_sprite_map[bldg.produces],bldg.x+1,bldg.y,bldg.produces,bldg.room)
    product.frames=1
    product.label=produces
    -- product.w = 0.5
    -- product.h = 0.5
    add(state.products,product)
    add(state.actors,product)
    if product.room==state.room then
        add(state.visible_sprites,product)
    end
    bldg.material=nil
end

function set_building_traits(bldg, sprite_id)
    bldg.sprite_id=sprite_id or cursor1.sprite_id
    bldg.label=production_map[bldg.sprite_id].name
    bldg.consumes=production_map[bldg.sprite_id].consumes
    bldg.produces=production_map[bldg.sprite_id].produces
end

function check_for_structure(x,y)
    sprite_id=mget(x+glb.structure_map_x,y)
    if (contains(building_ids,sprite_id)) return 'building'
    if (contains(belt_ids,sprite_id)) return 'belt'
    return ''
end

function update_product_pos_map()
    state.product_pos_map={}
    for product in all(state.products) do
        product_pos=prpos(flr(product.x),flr(product.y))
        state.product_pos_map[product_pos]=product
    end
end

function convert_building(x,y)
    local bldg=find_structure(x+0.5,y+0.5)
    if (bldg==nil) printh('error getting bldg at '..prpos(x,y)) return
    set_building_traits(bldg)
end
