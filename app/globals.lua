glb={
    MAP_W=16,
    MAP_H=16,
    -- grid cell unit size
    cell_size=8,
    inv_x=1,
    inv_y=7.5,
    tool_x=15,
    max_frames=60,
    max_products=200,
    structure_map_x=16,
    player_sprite_id=64,
}
