function getnext(start,coll,rev,idx)
    local starti=1
    local endi=#coll
    local iter=1
    if (rev) starti=#coll endi=1 iter=-1
    init=coll[starti]
    get_next=nil
    result=nil
    for i=starti,endi,iter do
        if (result!=nil) break
        if (get_next) result=coll[i] idx=i
        if (idx==nil) get_next=tostr(start)==tostr(coll[i]) else get_next=start==i
    end
    if (result !=nil) return idx,result else return starti,init
end

function copy_sprite(sprite,new_id,new_label)
    new_sprite = actor:new(sprite.sprite_id, sprite.x, sprite.y, sprite.name)
    if (new_id)    new_sprite.sprite_id = new_id
    if (new_label) new_sprite.label = new_label
    if (direction) new_sprite.direction = sprite.direction
    return new_sprite
end

function contains(set,key) return set[key]~=nil end
function is_empty(t) return next(t)==nil end
function tbl_len(T,name) printh(name..' cnt '..#T) end
function tbl_keys(T,name) for k,v in pairs(T) do printh(k..' '..tostr(v)) end end
function convert_to_pixels(x,y) return x*glb.MAP_W,y*glb.MAP_H end
function prpos(x,y) return x..','..y end
function getvec(a) return a.x,a.y end
function contains_attr(set,attr,val) for i,v in pairs(set) do if (v[attr]==val) return i,v or {} end end
