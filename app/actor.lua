function add_actor(sprite_id,x,y,name)
    local a = actor:new(sprite_id,x,y,name)
    add(state.actors,a)
    return a
end

function draw_actor(a)
    a:draw()
end

function move_actor(a)
    a:move()
end

-- make an actor
-- and add to global collection
-- x,y means center of the actor
-- in map tiles
actor={}

function actor:new(sprite_id,x,y,name,room)
    printh('new actor inputs '..prpos(x,y)..', '..name)
    local a={
        sprite_id=tonum(sprite_id),
        x=x,
        y=y,
        -- get relative position for drawing purposes
        rel_x=x%glb.MAP_W,
        rel_y=y%glb.MAP_H,
        room_x=state.room_x,
        room_y=state.room_y,
        room=prpos(state.room_x,state.room_y),
        name=name,
        label=name..': no label',
        dx=0,
        dy=0,
        frame=0,
        t=0,
        friction=0.35,
        bounce=0.4,
        direction_x=1,
        direction_y=0,
        frames=2,
        stationary=false,
        count=0,
        -- half-width and half-height
        -- slightly less than 0.5 so
        -- that will fit through 1-wide
        -- holes.
        w=0.4,
        h=0.4,
        age=time(),
    }
    if room~=nil then
        a.room_x=split(room)[1]
        a.room_y=split(room)[2]
        a.room=room
    end
    setmetatable(a,self)
    self.__index=self
    return a
end

function actor:move()
    -- on a belt?
    belt_dir_x,belt_dir_y=get_belt_effect(self)
    self.dx+=belt_dir_x
    self.dy+=belt_dir_y

    -- against a solid map object?
    if next_is_solid(self,self.dx,0) then
        self.dx*=-self.bounce
    end
    if next_is_solid(self,0,self.dy) then
        self.dy*=-self.bounce
    end

    if self.dx!=0 or self.dy!=0 then
        check_for_solid_actor(self)
    end

    -- add movement speed to position
    self.x+=self.dx
    self.y+=self.dy

    self.frame+=abs(self.dx)
    self.frame+=abs(self.dy)

    -- add passive animation
    -- what is t for
    self.frame+=0.1
    self.frame%=self.frames
    self.t+=1

    -- apply friction
    -- (comment for no inertia)
    self.dx*=(1-self.friction)
    self.dy*=(1-self.friction)
    -- without this damping dx/dy will never go below 0.0001
    -- which causes issues with above checks for whether to look for a collision
    if (abs(self.dx)<0.001) self.dx=0
    if (abs(self.dy)<0.001) self.dy=0
    self.rel_x=self.x%glb.MAP_W
    self.rel_y=self.y%glb.MAP_H
    self:update_room()
end

function actor:draw()
    if (self.sprite_id==nil) return
    local sx=(self.rel_x*glb.cell_size-glb.cell_size/2)
    local sy=(self.rel_y*glb.cell_size-glb.cell_size/2)
    if (self.draw_backing!=nil) self:draw_backing(sx,sy)
    local flipy=self.direction_y==-1
    if (self.name=='player') flipy=false
    spr(self.sprite_id+self.frame,sx,sy,1,1,self.direction_x==-1,flipy)
    if (self.count>1) print(self.count,sx,sy+4,7)
end

function actor:draw_backing(sx,sy)
    if (self.backing==nil) return else palt(0,false) spr(16,sx,sy,1,1) palt(0,true)
    local scl,plaque=1,self.backing
    if self.selected then
        scl=2
        plaque=self.backing_big
        showtext(self.name,64,56,7)
        if (contains(structure_names,self.name)) showtext(self.label,64,64,7)
    end
    for i,v in pairs(plaque) do
        local xo,yo=scl,scl
        if (i==1) xo=-scl yo=-scl
        if (i==2) yo=-scl
        if (i==3) xo=-scl
        spr(v,sx+xo,sy+yo,1,1)
    end
end

function actor:update_room()
    self.room_x=flr(self.x/glb.MAP_W)
    self.room_y=flr(self.y/glb.MAP_H)
    local old=self.room
    self.room=prpos(self.room_x,self.room_y)
    if (old!=self.room and self.room==state.room) add(state.visible_sprites,self)
end

function actor:update_label()
    if (self.name=='belt') self.label=belt_directions[self.sprite_id] self.direction=self.label
    if (self.name=='building') self.label=production_map[self.sprite_id].name
end
