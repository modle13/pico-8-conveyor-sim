cam={}

function cam:new()
    local o=actor:new(60, 22, 8, 'player')
    setmetatable(o, self)
    self.__index = self
    return o
end

function cam:update()
    -- if player 0-127, then 0, if player 128-255, then 1, etc.
    if state.tutorial_enabled then
        --set_room(7, 0)
    else
        local adj_rmx=flr(player1.x/glb.MAP_W)-state.room_x
        local adj_rmy=flr(player1.y/glb.MAP_H)-state.room_y
        if (adj_rmx != 0) update_room('x', adj_rmx)
        if (adj_rmy != 0) update_room('y', adj_rmy)
    end
end
