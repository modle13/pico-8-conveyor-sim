function handle_coordinates_debug()
    if (not state.debug) return
    if (not state.show_coords) return

    local cursor_pos_text=flr(cursor1.x)..','..flr(cursor1.y)
    print(cursor_pos_text,cursor1.x*8-8,cursor1.y*8-8)

    local player_pos_text=flr(player1.rel_x)..','..flr(player1.rel_y)
    print(player_pos_text,player1.rel_x*8-8,player1.rel_y*8-8)

    local room_pos_text='rm: '..player1.room_x..','..player1.room_y
    print(room_pos_text,player1.rel_x*8-12,player1.rel_y*8+4)

    print('MEM',9.5*glb.cell_size,9)
    print(stat(0),12*glb.cell_size,9)

    print('CPU TOT',7.5*glb.cell_size,17)
    print(stat(1),12*glb.cell_size,17)

    print('CPU SYS',7.5*glb.cell_size,25)
    print(stat(2),12*glb.cell_size,25)
end
